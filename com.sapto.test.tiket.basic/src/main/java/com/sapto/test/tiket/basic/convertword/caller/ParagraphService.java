package com.sapto.test.tiket.basic.convertword.caller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParagraphService {

	private Map<String, List<Integer>> wordMapper;

	public ParagraphService(Map<String, List<Integer>> wordMapper) {
		this.wordMapper = wordMapper;
	}

	// Jumlah kata dari teks di atas �
	public int getTotalWords() {

		int totalWord = 0;
		for (Map.Entry<String, List<Integer>> entry : wordMapper.entrySet()) {
			List<Integer> value = entry.getValue();
			totalWord = totalWord + value.size();
		}

		return totalWord;

	}

	// Jumlah kemunculan tiap kata dari teks di atas
	public Map<String, Integer> getTotalEachWord() {

		Map<String, Integer> mapEachWord = new HashMap<>();

		for (Map.Entry<String, List<Integer>> entry : wordMapper.entrySet()) {
			String key = entry.getKey();
			List<Integer> value = entry.getValue();
			Integer total = value.size();
			mapEachWord.put(key, total);
		}

		return mapEachWord;

	}

	// Jumlah kata yang hanya muncul satu kali
	public int wordWithAppearance(int minimal) {
		Map<String, Integer> mapEachWord = getTotalEachWord();
		int wordAppearance = 0;

		for (Map.Entry<String, Integer> entry : mapEachWord.entrySet()) {
			Integer countWord = entry.getValue();
			if (countWord == minimal) {
				wordAppearance++;
			}
		}

		return wordAppearance;
	}

	
	// Jumlah kata yang paling banyak muncul dan katanya �
	public Map<String, Integer> getHighestFreq() {

		Map<String, Integer> mapEachWord = getTotalEachWord();

		int maxAppearance = Collections.max(mapEachWord.values());
		Map<String, Integer> maxEntry = new HashMap<>();

		for (Map.Entry<String, Integer> entry : mapEachWord.entrySet()) {

			if (entry.getValue() == maxAppearance) {
				maxEntry.put(entry.getKey(), entry.getValue());
			}
		}

		return maxEntry;

	}

	// Jumlah kata yang paling sedikit muncul dan katanya	
	public Map<String, Integer> getLowestFreq() {

		Map<String, Integer> mapEachWord = getTotalEachWord();

		int minAppearance = Collections.min(mapEachWord.values());
		Map<String, Integer> min = new HashMap<>();

		for (Map.Entry<String, Integer> entry : mapEachWord.entrySet()) {

			if (entry.getValue() == minAppearance) {
				min.put(entry.getKey(), entry.getValue());
			}
		}

		return min;

	}

	public Map<String, List<Integer>> getWordMapper() {
		return wordMapper;
	}

	public void setWordMapper(Map<String, List<Integer>> wordMapper) {
		this.wordMapper = wordMapper;
	}

}
