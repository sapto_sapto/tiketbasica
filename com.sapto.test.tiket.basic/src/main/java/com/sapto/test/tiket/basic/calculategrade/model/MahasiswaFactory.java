package com.sapto.test.tiket.basic.calculategrade.model;

public interface MahasiswaFactory {

	public abstract Mahasiswa getMahasiswa(String nama, String nim, Integer absenceGrade, Integer midTestGrade,
			Integer uasTestGrade);

}
