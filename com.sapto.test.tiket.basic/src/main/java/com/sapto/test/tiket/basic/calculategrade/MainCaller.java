package com.sapto.test.tiket.basic.calculategrade;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.sapto.test.tiket.basic.calculategrade.model.Mahasiswa;
import com.sapto.test.tiket.basic.calculategrade.model.MahasiswaFactory;
import com.sapto.test.tiket.basic.calculategrade.services.PrinterData;

public class MainCaller {

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Masukan Jumlah Mahasiswa : ");
		int totalMahasiwa = scanner.nextInt();
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		MahasiswaFactory mahasiswaFactory = Mahasiswa::new;

		List<Mahasiswa> mahasiswas = new ArrayList<>();

		int counter = 0;

		do {

			// System.out.printf("Masukkan Data Mahasiswa ke %s%n ", counter + 1);

			System.out.print("Masukkan NIM : ");
			String nim = scanner.next();

			System.out.print(" Masukkan Nama : ");
			String nama = scanner.next();

			System.out.print(" Masukkan Nilai Kehadiran : ");
			int absence = scanner.nextInt();

			System.out.print(" Masukkan Nilai Midtest : ");
			int midTest = scanner.nextInt();

			System.out.print(" Masukkan Nilai UAS : ");
			int uasTest = scanner.nextInt();

			counter++;

			Mahasiswa mahasiswa = mahasiswaFactory.getMahasiswa(nim, nama, absence, midTest, uasTest);
			mahasiswas.add(mahasiswa);

		} while (counter < totalMahasiwa);

		PrinterData printerData = new PrinterData(mahasiswas);
		try {
			printerData.printFile();
		} catch (Exception e) {
			System.out.println("print data failed : ");
			e.printStackTrace();
		}

	}

}
