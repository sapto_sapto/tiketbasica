package com.sapto.test.tiket.basic.calculategrade.services;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.sapto.test.tiket.basic.calculategrade.model.Mahasiswa;

public class PrinterData {

	private List<Mahasiswa> mahasiswas;
	private PrintWriter printWriter;

	public PrinterData(List<Mahasiswa> mahasiswas) {
		this.mahasiswas = mahasiswas;
	}

	public List<Mahasiswa> getMahasiswas() {
		return mahasiswas;
	}

	public void setMahasiswas(List<Mahasiswa> mahasiswas) {
		this.mahasiswas = mahasiswas;
	}

	public void printConsole() {
		AtomicInteger index = new AtomicInteger();

		System.out.println("No. NIM Nama Nilai Akhir Grade");
		String separator = "====================================================================";
		System.out.println(separator);
		AtomicInteger totalPass = new AtomicInteger();
		mahasiswas.forEach(item -> {
			index.getAndIncrement();

			System.out.println(index + ". " + item.printOutData());
			if (item.isPassExam()) {
				totalPass.getAndIncrement();
			}

		});

		System.out.println(separator);

		System.out.printf(" Jumlah Mahasiswa : %s%n (berdasarkan hasil kalkulasi) ", mahasiswas.size());
		System.out.printf(" Jumlah Mahasiswa yg lulus: %s%n (berdasarkan hasil kalkulasi) ", totalPass);
		System.out.printf(" Jumlah Mahasiswa yg tidak lulus: %s%n (berdasarkan hasil kalkulasi) ",
				mahasiswas.size() - totalPass.intValue());

	}

	public void printFile() throws IOException {
		
		FileWriter fileWriter = new FileWriter("C:\\datamhs.txt");
	    printWriter = new PrintWriter(fileWriter);
		AtomicInteger index = new AtomicInteger();

		printWriter.println("No. NIM Nama Nilai Akhir Grade");
		String separator = "====================================================================";
		printWriter.println(separator);
		AtomicInteger totalPass = new AtomicInteger();
		mahasiswas.forEach(item -> {
			index.getAndIncrement();

			printWriter.println(index + ". " + item.printOutData());
			if (item.isPassExam()) {
				totalPass.getAndIncrement();
			}

		});

		printWriter.println(separator);

		printWriter.printf(" Jumlah Mahasiswa : %s%n (berdasarkan hasil kalkulasi) ", mahasiswas.size());
		printWriter.printf(" Jumlah Mahasiswa yg lulus: %s%n (berdasarkan hasil kalkulasi) ", totalPass);
		printWriter.printf(" Jumlah Mahasiswa yg tidak lulus: %s%n (berdasarkan hasil kalkulasi) ",
				mahasiswas.size() - totalPass.intValue());
		printWriter.close();

	}

}
