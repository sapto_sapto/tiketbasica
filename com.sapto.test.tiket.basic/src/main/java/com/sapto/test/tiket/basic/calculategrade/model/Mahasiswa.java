package com.sapto.test.tiket.basic.calculategrade.model;

public class Mahasiswa {

	private String nama;

	private String nim;

	private Integer absenceGrade;

	private Integer midTestGrade;

	private Integer uasTestGrade;

	public Mahasiswa(String nim, String nama, Integer absenceGrade, Integer midTestGrade, Integer uasTestGrade) {
		this.nama = nama;
		this.nim = nim;
		this.absenceGrade = absenceGrade;
		this.midTestGrade = midTestGrade;
		this.uasTestGrade = uasTestGrade;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public Integer getAbsenceGrade() {
		return absenceGrade;
	}

	public void setAbsenceGrade(Integer absenceGrade) {
		this.absenceGrade = absenceGrade;
	}

	public Integer getMidTestGrade() {
		return midTestGrade;
	}

	public void setMidTestGrade(Integer midTestGrade) {
		this.midTestGrade = midTestGrade;
	}

	public Integer getUasTestGrade() {
		return uasTestGrade;
	}

	public void setUasTestGrade(Integer uasTestGrade) {
		this.uasTestGrade = uasTestGrade;
	}

	public double calculateFinalExam() {
		double finalExam = 0.2 * absenceGrade + 0.4 * midTestGrade + 0.4 * uasTestGrade;
		return finalExam;
	}

	public String calculateGrade() {
		double finalExam = calculateFinalExam();
		if (finalExam <= 45) {
			return "E";
		} else if (finalExam <= 60) {
			return "D";
		} else if (finalExam <= 75) {
			return "C";
		} else if (finalExam <= 84) {
			return "B";
		} else {
			return "A";
		}
	}
	
	public String printOutData() {
		return nim+" "+nama+" "+ calculateFinalExam() +" " + calculateGrade();
	}
	
	public boolean isPassExam() {
		
		if(calculateGrade().equals("D") || calculateGrade().equals("E")) {
			return false;
		}else {
			return true;
		}
	}

}