package com.sapto.test.tiket.basic.convertword;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordConverter implements IWordConverter {

	@Override
	public String inverseWord(String input) {
		String[] words = input.split("\\W+");
		for (int j = 0; j < words.length; j++) {
			String word = words[j];
			char[] characters = word.toCharArray();
			for (int i = 0; i < characters.length; i++) {
				char currentChar = characters[i];
				boolean isUpperCase = Character.isUpperCase(currentChar);
				if (isUpperCase) {
					characters[i] = Character.toLowerCase(currentChar);
				} else {
					characters[i] = Character.toUpperCase(currentChar);
				}

			}

			words[j] = String.valueOf(characters);
		}

		String finalWord = String.join(" ", words);

		return finalWord;
	}

	@Override
	public String removeVocal(String input) {
		String[] words = input.split("\\W+");
		for (int j = 0; j < words.length; j++) {
			String word = words[j];
			char[] characters = word.toCharArray();
			List<String> result = new ArrayList<>();
			for (int i = 0; i < characters.length; i++) {
				char currentChar = characters[i];
				boolean isNotVowel = "AEIOUaeiou".indexOf(currentChar) == -1;
				if (isNotVowel) {

					result.add(Character.toString(currentChar));
				}
			}

			words[j] = joinWord(result);

		}

		String finalWord = String.join(" ", words);
		return finalWord;
	}

	@Override
	public String convertWord(String input) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, List<Integer>> calculateParagraph(String input) throws IOException {
		String[] words = input.split("\\W+");
		
		Map<String, List<Integer>> wordMapper = new HashMap<>();

		for (int i = 0; i < words.length; i++) {
			String word = words[i];
			List<Integer> indexs = wordMapper.get(word);
			if(indexs == null) {
				indexs = new ArrayList<>();
			}
			indexs.add(i);
			wordMapper.put(word, indexs);
		}

		return wordMapper;
	}

	private String joinWord(List<String> words) {
		StringBuilder builder = new StringBuilder();
		for (String s : words) {
			builder.append(s);
		}
		String str = builder.toString();

		return str;
	}

}
