package com.sapto.test.tiket.basic.convertword;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface IWordConverter {
	
	public String inverseWord(String input);
	
	public String removeVocal(String input);
	
	public String convertWord(String input);
	
	public Map<String, List<Integer>> calculateParagraph(String input) throws IOException;

}
